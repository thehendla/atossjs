var casper = require('casper').create({
	verbose: false,
	logLevel: "warn",
	exitOnError: false,
	pageSettings: {
		userAgent: "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36"
	}
});

if (casper.cli.args.length != 2) {
	casper.echo("\nUsage:casperjs atoss-balance.js <login> <password>").exit();
};

var login = casper.cli.get(0);
var password = casper.cli.get(1);

var tmpDirPrefix = '/tmp/';

casper.options.viewportSize = {width: 1000, height: 1500};

casper.start("http://rze-pep.rze.de/SES/web");

casper.then(function() {
	this.page.switchToChildFrame(0);
	this.fillSelectors('#DefaultForm', {
		'input[id="logonid"]': login,
		'input[id="password"]': password,
	}, false);
	this.click('#ok');
}).wait(2000);

casper.then(function() {
	this.page.switchToChildFrame(0);
	casper.waitForSelector('table[id="container_liste_zemstimecard_gridpart-cave"]',
		function() {
			casper.capture(tmpDirPrefix + login + "_balance_success.png");
			
			var trLength = casper.getElementsInfo('table[id="container_liste_zemstimecard_gridpart-cave"] tbody[class="z-rows"] tr[tabindex="0"]').length;
			var balanceYesterday = this.fetchText('table[id="container_liste_zemstimecard_gridpart-cave"] tbody[class="z-rows"] tr[tabindex="0"]:nth-child(' + (trLength-1) + ') td:nth-child(10) span');
			var bookingPairs = [];
			try{
				var bookingRows = casper.getElementsInfo('table[id="container_liste_zemstimecard_gridpart-cave"] tbody[class="z-rows"] tr[tabindex="0"]:nth-child(' + (trLength) + ') td:nth-child(9) div[class="z-grid-body"] tbody[class="z-rows"] tr').map(function(bookingRow) {
					if (bookingRow.text && bookingRow.text.trim !== "") {
						bookingPair = {};
						bookingRow = bookingRow.text.trim().replace(/k/g, ' ').split(' ');
						if ( bookingRow[1].substr(-1) === "a" ) {
							bookingPair['isEndAutomatic'] = true;
						}
						bookingPair['start'] = bookingRow[0];
						bookingPair['end'] = bookingRow[1].replace(/a/g, '');
						bookingPairs.push(bookingPair);
					}
				});
			} catch(e) {
			}

			var responseJson = {};
			//responseJson['tableRowCount'] = trLength;
			responseJson['success'] = true;
			responseJson['balanceYesterday'] = balanceYesterday;
			responseJson['bookingPairsToday'] = bookingPairs;
			casper.echo(JSON.stringify(responseJson, null, 2));
		},
		function() {
			casper.capture(tmpDirPrefix + login + "_balance_failure.png");

			var responseJson = {};
			responseJson['success'] = false;
			responseJson['message'] = "No bookings table found before timeout.";
			casper.echo(JSON.stringify(responseJson, null, 2));
		},
		10000
	);
});

casper.run();
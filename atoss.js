var atoss_url = 'http://rze-pep.rze.de/SES/time'

var casper = require('casper').create({
	verbose: true,
	logLevel: "debug",
	viewportSize: {width: 700, height: 560},
	exitOnError: true,
	pageSettings: {
		userAgent: "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36"
	}
});

var atoss_personalnumber = casper.cli.args[0];
var atoss_pincode = casper.cli.args[1];

if(atoss_pincode == 0) {
	atoss_pincode = '000';
}

if(casper.cli.args.length != 2) {
	casper.echo('usage: casperjs atoss.js <personalnumber> <pincode>');
	casper.exit();
} else {
	casper.echo('atoss_personalnumber <- ' + atoss_personalnumber);
	casper.echo('atoss_pincode <- ' + atoss_pincode);
}

var datePrefix = new Date().toISOString();
var tmpDirPrefix = '/tmp/';

var load_form = false;
var atoss_success = false;

casper.start(atoss_url);

casper.then(function() {
    this.echo('Opened Page: ' + this.getTitle());
});

casper.then(function() {
	this.echo('switch to iframe');
	this.page.switchToChildFrame(0);

	this.echo('wait for selector #DefaultForm');
	casper.waitForSelector('#DefaultForm', function() {
		this.echo('found form insert personalnumber and pin');
		this.fillSelectors('#DefaultForm', {
			'input[id="badgeno"]': atoss_personalnumber,
			'input[id="pincode"]': atoss_pincode,
		}, false);
		this.echo('sendform');
		this.click('#ok');
		load_form = true;
	}, function() { this.echo('timeout: cannot load form #DefaultForm'); }, 5000);
});

var getAttributePrefix = function() {
	return casper.evaluate(function() {
		return '#' + document.getElementById('DefaultForm').firstChild.id.substr(0,4);
	});
}

casper.then(function() {
	if(load_form) {
		var successID = '#time2_top_container';
	
		var attrPrefix = getAttributePrefix();
		var personalID = attrPrefix + 'q1';
		var nameID = attrPrefix + 'r1';
		var timeID = attrPrefix + 's1';
		var dateID = attrPrefix + 'y1';

		this.echo('wait for selector ' + successID);
		casper.waitForSelector(successID, function() {
			atoss_success = true;
			this.echo('successfull login/logout');
		
			var res_personal = this.fetchText(personalID);
			var res_name = this.fetchText(nameID);
			var res_time = this.fetchText(timeID);
			var res_date = this.fetchText(dateID);
	
			this.echo('personalnumber: ' + res_personal);
			this.echo('name: ' + res_name);
			this.echo('time: ' + res_time);
			this.echo('date: ' + res_date);
		
			this.capture(tmpDirPrefix + datePrefix + '_success.png');
			copyFile('_success.png');

		}, function() { this.echo('timeout: cannot find success container #time2_top_container'); }, 10000);
	}
});

casper.then(function() {
	if(load_form && !atoss_success) {
		this.echo('wait for selector #statusLabel');
		casper.waitForSelector('#statusLabel', function() {
			this.echo('found statusLabel');
			var res_status = this.fetchText('#statusLabel');
			this.echo('status: ' + res_status);
		
			this.capture(tmpDirPrefix + datePrefix + '_failure.png');
			copyFile('_failure.png');
		}, function() { this.echo('timeout: cannot find error #statusLabel'); } , 1000);
	}
});


casper.then(function() {
	this.wait(1000, function() {
		this.echo('write debug info');
		var fs = require('fs');
		fs.write(tmpDirPrefix + datePrefix + '_debug.txt', this.getHTML() , 'a');
		this.capture(tmpDirPrefix + datePrefix + '_debug.png');

		copyFile('_debug.txt');
		copyFile('_debug.png');

	})
});

function copyFile(filename) {
	var fs = require('fs');
	fs.copy(tmpDirPrefix + datePrefix  + filename, tmpDirPrefix + atoss_personalnumber + filename);

}


casper.run();
